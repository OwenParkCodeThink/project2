#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "word.h"

static void test_scrabble_score(void)
{
	assert(word_score(as_word("quirky", "")) == 22);
	assert(word_score(as_word("a", "")) == 1);
	assert(word_score(as_word("A", "")) == 1);
	assert(word_score(as_word("zoo", "")) == 12);
	assert(word_score(as_word("street", "")) == 6);
	assert(word_score(as_word("at", "")) == 2);
	assert(word_score(as_word("OxyphenButazone", "")) == 41);
	assert(word_score(as_word("abcdefghijklmnopqrstuvwxyz", "")) == 87);

	assert(word_score(as_word("scotland", "d--3--2-")) == 28);
	assert(word_score(as_word("bar", "t33")) == 27);
	assert(word_score(as_word("SWITCH", "")) == 14);

	board_word_t word = as_word("bar", "t33");
	word.square[1].tile.is_blank = true;
	assert(word_score(word) == 18);
}


static void test_scrabble_score_bad(void)
{
	assert(word_score(as_word("quirky", "----------------")) == 22);
	assert(word_score(as_word("", "")) == 0);

	assert(word_score(as_word("scotland", "d--3--")) == 26);
}


static void test_is_word(void)
{
	assert(is_word(as_word("abc", ""), "./data/word_list.txt"));
	assert(is_word(as_word("hello", ""), "./data/word_list.txt"));
	assert(is_word(as_word("absence", ""), "./data/word_list.txt"));
	assert(!is_word(as_word("absences", ""), "./data/word_list.txt"));
	assert(is_word(as_word("scenario", ""), "./data/word_list.txt"));
	assert(is_word(as_word("zus", ""), "./data/word_list.txt"));
}


static void test_is_word_bad(void)
{
	assert(!is_word(as_word("hello", ""), NULL));
}


static void test_as_word(void)
{
	board_word_t word = as_word("quirky", "");
	assert(word.length == 6);
	assert(word.square[0].tile.face == 'Q');
	assert(word.square[5].tile.face == 'Y');

	word = as_word("scotland", "d--3--2----------");
	assert(word.length == 8);
}


int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;

	printf("Running tests...\n\n");

	test_scrabble_score();
	test_is_word();
	test_scrabble_score_bad();
	test_is_word_bad();
	test_as_word();

	printf("\nAll tests passed!!!\n");
}
