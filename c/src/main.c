#include <stdbool.h>
#include <stdio.h>

#include "board.h"
#include "tiles.h"
#include "ui.h"
#include "word.h"

#define WORD_LIST_FILE "./data/word_list.txt"

/* Get the words made in the turn, check if each are valid, if they are then
 * score the words and add return the score for the turn. On fail returns 0.
 */
static unsigned score_turn(const char* word_list_file)
{
	if (!word_list_file) return 0;

	size_t word_count = TILE_RACK_SIZE + 1;
	board_word_t word[TILE_RACK_SIZE + 1] = {0};

	if (!board_turn_get_words(word, &word_count))
		return 0;

	unsigned score = 0, score_word = 0;
	bool success = true;
	for (unsigned i = 0; i < word_count; i++)
	{
		if (!is_word(word[i], word_list_file)
		    || ((score_word = word_score(word[i])) == 0))
		{
			success = false;
			break;
		}
		score += score_word;
	}

	if (!success)
	{
		// TODO: identify bad word and flag in ui
		score = 0;
	}
	return score;
}


/* Mainloop of program, contains all of the state that neads to be shared
 * between the components */
int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;

	/* Create the tile bag and rack - filling the latter from the former */
	tile_bag_t* tile_bag = tile_bag_create();
	if (!tile_bag)
		return 1;

	tile_rack_t rack = tile_rack_create();
	tile_rack_topup(&rack, tile_bag);

	/* Initialise the board and the ui */
	board_init(15);
	ui_init(&rack);

	/* Initialise the score and quit states */
	unsigned running_score = 0;
	bool     quit          = false;
	while (true)
	{
		/* Blocking call that waits for the ui to issue a command */
		ui_command_t cmd = ui_get_cmd();

		/* Main behavioural logic for program */
		switch (cmd.type)
		{
			case UI_COMMAND_MODE:
				ui_set_mode(cmd.data.mode);
				break;

			case UI_COMMAND_MV:
				ui_set_mode(UI_MODE_ON_BOARD);
				ui_move(cmd.data.direction);
				break;

			case UI_COMMAND_ENTER:
			{
				/* Attempt to complete the turn, on fail - show user the
				 * error */
				unsigned score = score_turn(WORD_LIST_FILE);
				if (score == 0)
					ui_print_error("Invalid turn");
				else
				{
					running_score += score;
					ui_update_score(running_score);
					board_turn_complete();
					tile_rack_topup(&rack, tile_bag);
				}
				break;
			}

			case UI_COMMAND_ESC:
			{
				/* Clear turn and return tiles to rack */
				ui_set_mode(UI_MODE_ON_BOARD);
				board_square_t current_square;
				while (board_turn_pop(&current_square, NULL))
					tile_rack_add(&rack, current_square.tile);
				break;
			}

			case UI_COMMAND_DEL:
			{
				/* Delete tile at cursor position, if successful, push tile
				 * back onto rack */
				ui_set_mode(UI_MODE_ON_BOARD);
				board_square_t square;
				if (board_turn_pop_position(cmd.data.cmd_pos, &square))
					tile_rack_add(&rack, square.tile);
				break;
			}

			case UI_COMMAND_ADD_TILE:
			{
				/* Add tile to board (tracked in the turn) if it's available
				 * on rack */
				tile_t tile;
				if (tile_rack_remove(&rack, cmd.data.cmd_tile, &tile))
				{
					if (!board_add_tile(tile, cmd.data.cmd_pos))
						tile_rack_add(&rack, tile);
				}
				else
					ui_print_error("%c tile not in rack", cmd.data.cmd_tile);
				break;
			}

			case UI_COMMAND_RERACK:
				/* Put all tiles on rack back into bag and get a new selection */
				ui_set_mode(UI_MODE_ON_BOARD);
				tile_bag_add(tile_bag, rack.tile, rack.size);
				tile_bag_shuffle(tile_bag);
				tile_rack_clear(&rack);
				tile_rack_topup(&rack, tile_bag);
				break;

			case UI_COMMAND_QUIT:
				/* Quit program */
				quit = true;
				break;

			default:
				break;
		}
		if (quit)
			break;

		/* Update the ui with the new game state - note that it only draws
		 * the changes to the board */
		ui_draw();
	}

	/* Clean up resources and exit */
	tile_bag_delete(tile_bag);
	ui_tear_down();
	return 0;
}
